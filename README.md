# Mike Jang's Qualifications

Mike Jang's qualifications for the Hidden Layer Technical Writing position:

- Resume: mjangResume_hiddenlayer.docx
- Writing Samples: writingSamples.md
- Cover Letter: coverLetter_hiddenlayer.docx
- Speaking Experience: speakingExperience.md
- LinkedIn Profile: https://www.linkedin.com/in/mijang/
